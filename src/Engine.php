<?php namespace SarojPant\EasyTemplate;

use SarojPant\EasyTemplate\Exception\FileNotFoundException;
use SarojPant\EasyTemplate\Exception\DirectoryNotFoundException;

class Engine
{
    /**
     * @var string directory where templates are located
     */
    protected $templateDir;

    /**
     * @var string default template extension
     */
    protected $ext = '.php';

    /**
     * @var array data that will be available in template files
     */
    protected $data = [];

    /**
     * @var array custom sections
     */
    protected $sections = [];

    /**
     * @var array used internally by this class
     */
    protected $currentSection = [];

    /**
     * @var null used internally by this class
     */
    protected $rootLayout = null;

    /**
     * @var bool
     */
    protected $templatesGathered = false;

    /**
     * Engine constructor.
     *
     * @param string $templateDir directory where template file are located
     * @param string $templateExtension default template extension
     * @throws DirectoryNotFoundException
     */
    public function __construct($templateDir, $templateExtension = '.php')
    {
        $this->templateDir = realpath($templateDir);
        if (!file_exists($this->templateDir)) {
            throw new DirectoryNotFoundException($templateDir . ' not found.');
        }

        $this->ext = $templateExtension;
    }
    
    /**
     * Reset instance
     */
    private function reset()
    {
        $this->data = [];
        $this->sections = [];
        $this->currentSection = [];
        $this->rootLayout = null;
        $this->templatesGathered = false;
    }

    /**
     * Return or echoes the template
     *
     * @param string $templateName template name to render
     * @param array $data data for template files
     * @param bool $echo whether to echo or return output
     *
     * @return string|null
     */
    public function render($templateName, array $data = [], $echo = true)
    {
        // Prevent data corruption from previous calls (if any)
        $this->reset();
        
        $this->data = $data;

        // For now let's assume this is the root template
        $this->rootLayout = $templateName;

        // Step 1.
        // Let's include the template. As the template is parsed, the template
        // automatically handles inheritance and related inserts (if any).
        // We will not output anything as yet. You ask why ;) - see step 2.
        ob_start();
        $this->includeTemplate($templateName);
        ob_get_clean();

        // Set the templates gathered flag
        $this->templatesGathered = true;

        // Step 2.
        // Step 1 has already collected all templates and section content and set the
        // root layout. The root layout has everything in order which is why we did
        // not output anything in step 1. Let's just echo/return the root layout
        // contents now. Simple - huh :)
        ob_start();
        $this->includeTemplate($this->rootLayout);
        $content = ob_get_clean();

        if (!$echo) {
            return $content;
        }

        echo $content;
        return null;
    }

    /**
     * Includes the given template file
     *
     * @param $template
     * @throws FileNotFoundException
     */
    private function includeTemplate($template)
    {
        extract($this->data);
        include $this->getTemplateFilename($template);
    }

    /**
     * Returns template file path
     *
     * @param $template
     * @return string
     * @throws FileNotFoundException
     */
    private function getTemplateFilename($template)
    {
        // Convert '.' to '/' if found
        $template = str_replace('.', DIRECTORY_SEPARATOR, $template);

        $templateFile = rtrim($this->templateDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR
            .  rtrim($template, $this->ext) . $this->ext;

        if (!file_exists($templateFile)) {
            throw new FileNotFoundException("{$template} does not exist.");
        }

        return $templateFile;
    }

    /**
     * Inherits the current template from given layout
     *
     * @param string $template template path
     */
    public function layout($template)
    {
        $this->rootLayout = $template;

        // Check if the new root template is really root template and if it is, do not include it,
        // it should only run once.
        if (strpos(file_get_contents($this->getTemplateFilename($template)), '$this->layout') === false) {
            return;
        }

        $this->includeTemplate($template);
    }

    /**
     * Outputs section content
     *
     * @param string $section section name
     * @return string
     */
    public function section($section)
    {
        if (isset($this->sections[$section])) {
            echo $this->sections[$section];
        }

        return '';
    }

    /**
     * Includes another template in the current template
     *
     * @param string $template template path
     */
    public function insert($template)
    {
        $this->includeTemplate($template);
    }

    /**
     * Starts section buffering
     *
     * @param string $section section name
     */
    public function startSection($section)
    {
        $this->currentSection[] = $section;
        ob_start();
    }

    /**
     * Stores current section for future use. Sections can be nested.
     */
    public function stopSection()
    {
        if (empty($this->currentSection)) {
            throw new \ErrorException(get_class($this) . ": stopSection() called'
            . ' before or without startSection().");
        }

        $currentSection = array_pop($this->currentSection);

        if (array_key_exists($currentSection, $this->sections)) {
            if (!$this->templatesGathered) {
                $this->sections[$currentSection] = $this->sections[$currentSection] . ob_get_clean();
            } else {
                $this->sections[$currentSection] = ob_get_clean() . $this->sections[$currentSection];
            }
        } else {
            $this->sections[$currentSection] = ob_get_clean();
        }
    }

    /**
     * Returns variable data
     *
     * @param string $variableName
     * @param string $default
     * @param bool $escape
     *
     * @return string
     */
    public function get($variableName, $default = '', $escape = true)
    {
        // Return $default if variable not found
        if (!array_key_exists($variableName, $this->data)) {
            return $default;
        }

        $data = $this->data[$variableName];

        return $escape ? htmlspecialchars($data) : $data;
    }
}
