# EasyTemplate

EasyTemplate is a simple native PHP template engine.

## Features

* Template inheritance and sections
* Insert content to sections from anywhere
* Composer ready and PSR-2 compliant

## Install

Via Composer

``` bash
$ composer require saroj-pant/easy-template
```

or in your composer.json file
``` json
{
    "require": {
        "saroj-pant/easy-template": "^1.0"
    }
}
```

## Template inheritance and sections

### Template inheritance
Template inheritance allows a template to extend from another template. The child templates can add more content to the master template using "sections".

* A template can extend from another template using **$this->$layout()** method.
* You can do multiple levels of template nesting.

### Sections
Sections are used to group content together. The master template will define and output different sections (header, footer, sidebar etc.) and the child templates will add to those sections.

* A section is defined in the master template using **$this->section()** method.
* You can define and output a section in the same line **echo $this->section('header')**
* The **$this->startSection()** and **$this->stopSection()** methods are used to add content to a section like so
``` php
// Add content to header section
$this->startSection('header');
echo '<div class="header">Header content</div>';
$this->stopSection();

// Add content to footer section
$this->startSection('footer');
echo '<div class="footer">Footer content</div>';
$this->stopSection();
```

## Usage

### Rules/Limitations

* Rule 1: The child templates should always insert content to sections in the root template. Content which are not added to sections will not display.
* Rule 2: Rule 1 can be ignored only if the non-root template is loaded using **$this->insert()** method from the root template.

### Notes

* All the template methods are called using the **$this** pseudo-variable.
* The template file extension does not need to be passed when calling template methods
* '.' can be used as directory separator in template method argument for readability. Example **$this->include('partials.header')** is same as **$this->include('partials/header')**

### Loading templates

To load a template, we just need to initialize the template engine and call the **render()** method.


#### Step 1: Initialize engine constructor
The template engine is initialized by passing the template directory path as the first argument. In addition, you may wish to specify the template file extension as a second argument which is '.php' by default.

``` php
$templateDir = 'path/to/template/directory';
$templateEngine = new SarojPant\EasyTemplate\Engine($templateDir);
```
#### Step 2: Render template
The **render()** method accepts the template path as the first argument. The second and third arguments are optional and are used to pass data(array) and a boolean flag to display/return template content respectively. The data elements are shared as local variables across all templates.
``` php
$templateEngine->render('frontend.index', ['title' => 'Home'];
```

## Template methods

The template methods are available using $this pseudo-variable in template files.

### layout()
The layout() method is used to inherit from another template and expects the template path as an argument.

``` php
$this->layout('layout.1column'); // Recommended
$this->layout('layout/1column');
$this->layout('layout/1column.php');
```

### insert()
The insert() method will insert a template and expects the template path as an argument.
``` php
$this->insert('common.sidebar');
```

### section()
The section() method defines a section and is used in the root template.
``` php
$this->section('header');
$this->section('sidebar');
```

### startSection() and stopSection()
Content is added to a section by wrapping the content between the **startSection()** and **stopSection()** methods. The **startSection()** requires the section name as the only argument and the **stopSection()** does not have any arguments. The section content is automatically appended to existing content set by parent templates.
``` php
$this->startSection('header')
echo '<h1>This is header</h1>';
$this->stopSection();

$this->startSection('sidebar');
$this->insert('partials.sidebar_menu');
$this->stopSection();
```

### get()
The get method will return the contents of the given variable name. The second and third arguments are optional. The second argument accepts a default value. The third argument accepts a boolean flag to escape the output using **htmlspecialchars()** and is true by default.
``` php
// Outputs $pageHeading if found, empty string if not found
echo $this->get('pageHeading'); 

// Outputs 'Home' if pageHeading is not found
echo $this->get('pageHeading', 'Home'); 
```

## A simple template system example

The example below will load the login template

### Login page template

``` php
<!-- views/login.php -->
<?php
// Use the two column layout 
$this->layout('layout.2columns');

// Add content to the 'main_content' section
$this->startSection('main_content');
?>

<div class = "login-form">
	<!-- Form html -->
</div>

<?php
$this->stopSection();
```

### Two column layout template 
``` php
<!-- views/layout/2columns.php -->

<html>
    <head>
        <title>My App - <?= $this->get('title') ?></title>
    </head>
    <body>
		<!-- Outputs header content -->
		<?= $this->insert('header') ?>
		
		<!-- main content -->
		<div class="main-content">
			<?= $this->section('main_content') ?>
		</div>
    </body>
</html>
```

### Header template
``` php
<!-- views/header.php -->

<div class = "main-menu">
	<!-- Main Menu -->
</div>
```

### Loading and rendering the template
``` php
// Include the composer autoload
require 'vendor/autoload.php';

// Initialize the engine
$engine = new EasyTemplate(__DIR__ . DIRECTORY_SEPARATOR . 'views');

// Render login page
$engine->render('login', ['title' => 'Login', 'pageHeading' => 'Login']);
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Bugs/Issues

If you discover any issues, please report [here](https://bitbucket.org/sarojpant/easytemplate/issues).

## Credits

- [Saroj Pant](https://bitbucket.org/sarojpant)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.