# Changelog

### Version 1.1.0 (January 11, 2016)
- Added the get() method
- Ordered the section content to be displayed based on the template hierarchy.
- Improved documentation

### Version 1.0.0 (January 9, 2016)
- First stable release
