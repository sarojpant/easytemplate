# How to contribute

1. Fork this repository
2. Make your changes on a separate feature branch
3. Send a pull request from your feature branch to this repository's master branch.